import java.util.Scanner;

public class SchaltjahrCheck {
	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) { //Umgebung zur Annahme von Eingaben
			System.out.println("Jahr eingeben (nur Zahlen erlaubt, maximal |2147483647|)"); //Bittet um die Eingabe des Jahrs
			int jahr = input.nextInt(); //Nimmt die Eingabe des Jahrs entgegen und schreibt sie in die Variable jahr
			System.out.print("Das Jahr " + jahr + " ist, war oder wird "); //Gibt den ersten Teil des Resultats aus
			if((jahr % 4 == 0 && jahr % 100 > 0) || jahr % 400 == 0) { //Prueft, ob das Jahr ein Schaltjahr ist (durch 4 teilbar, aber nicht durch 100 ODER durch 400 teilbar)
				System.out.print("ein Schaltjahr (sein)."); //Falls ja: Positive Antwort
			}else {
				System.out.print("kein Schaltjahr (sein)."); //Falls nein: Negative Antwort			
			}
			
		}
	}

}